//
// Created by fifal on 2.4.17.
//

#include "rsa.h"

rsa::rsa() {
    srand(time(NULL));
}

long rsa::calc_euler(long &number_p, long &number_q) {
    return (number_p - 1) * (number_q - 1);
}

long rsa::get_random_number(long min, long max) {
    long output;
    output = min + (rand() % (max - min + 1));
    return output;
}

bool rsa::is_prime_number(long &number) {
    if (number <= 1) {
        return false;
    } else if (number <= 3) {
        return true;
    } else if (number % 2 == 0 || number % 3 == 0) {
        return false;
    } else {
        long i = 5;
        while (i * i <= number) {
            if (number % i == 0 || number % (i + 2) == 0) {
                return false;
            }
            i = i + 6;
        }
        return true;
    }
}

long rsa::find_number_e(long &number_euler) {
    long number_e = 2;

    // Dokud není číslo prvočíslo a zároveň není nejvyšší společní dělitel
    // s number_euler roven 1 generuj další číslo
    while (gcd(number_e, number_euler) != 1 || !is_prime_number(number_e)) {
        number_e++;
    }

    return number_e;
}

long rsa::mod_inverse(long &a, long &m) {
    a = a % m;
    for (long x = 1; x < m; x++)
        if ((a * x) % m == 1)
            return x;
}


long rsa::find_number_d(long &number_euler, long &number_e) {
    return mod_inverse(number_e, number_euler);
}


long rsa::get_random_prime(long min, long max) {
    auto number = get_random_number(min, max);

    // Dokud vygenerované číslo není prvočíslo generuj dál
    while (!is_prime_number(number)) {
        number = get_random_number(min, max);
    }
    return number;
}

long rsa::gcd(long a, long b) {
    for (;;) {
        if (a == 0) return b;
        b %= a;
        if (b == 0) return a;
        a %= b;
    }
}


void rsa::generate_keys(rsa::public_key &pub_key, rsa::private_key &priv_key) {
    auto time_start = std::chrono::system_clock::now();

    long number_p = get_random_prime(5, 25);
    long number_q = get_random_prime(5, 25);

    while (number_q == number_p) {
        number_q = get_random_prime(5, 25);
    }

    long number_n = number_p * number_q;

    if (number_n < 127) {
        generate_keys(pub_key, priv_key);
        return;
    }

    long number_euler = calc_euler(number_p, number_q);

    long number_e = find_number_e(number_euler);

    long number_d = find_number_d(number_euler, number_e);

    auto time_stop = std::chrono::system_clock::now();
    std::chrono::duration<double> diff = time_stop - time_start;


    pub_key.n = number_n;
    pub_key.e = number_e;

    priv_key.n = number_n;
    priv_key.d = number_d;
}

std::string rsa::encrypt(std::string text, const struct public_key p_key) {
    std::stringstream stream;
    stream << std::hex;
    std::string result(stream.str());
    for (char ch : text) {
        BigInteger enc = (int) ch;
        for (int i = 1; i < p_key.e; ++i) {
            enc *= ch;
        }
        enc = enc % p_key.n;

        stream << enc << " ";
    }

    return stream.str();
}

std::string rsa::decrypt(const std::string encrypted, const rsa::private_key priv_key) {
    auto time_start = std::chrono::system_clock::now();

    auto split = util::split(encrypted, " ");
    std::string decrypted;

    for (auto str : split) {
        long part;

        std::stringstream stream;
        stream << str;
        stream >> std::hex >> part;

        BigInteger big_part = part;
        for (int i = 1; i < priv_key.d; ++i) {
            big_part *= part;
        }
        big_part = big_part % priv_key.n;
        decrypted += (char) big_part.toInt();
    }

    auto time_stop = std::chrono::system_clock::now();
    std::chrono::duration<double> diff = time_stop - time_start;

    return decrypted;
}
